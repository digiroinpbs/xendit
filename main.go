package main

import (
	"net/http"
	"log"
	"io/ioutil"
	"fmt"
	"bytes"
	"math/rand"
	"time"
	"strconv"
	"github.com/buger/jsonparser"
	"strings"
	"errors"
	"github.com/magiconair/properties"
	"github.com/go-redis/redis"
)

func initParam()map[string]string{
	p := properties.MustLoadFile("/etc/service.conf", properties.UTF8)
	return map[string]string{
		"apiKey": "xnd_production_OIiIfOUhhrP7k8NhebUeHTOYMtGmodF4wyHl+R1q9GfV8L2oDAV2gg==",
		"apiKeySms": "143c42ef76ee0c4b0109cdd343f4e79e",
		//"apiKey": "xnd_development_P4yAfOUhhrP7k8NhebUeHTOYMtGmodF4wyHl+R1q9GfV8L2oDAV3hg==",
		"url": "https://api.xendit.co/",
		"urlSms": "http://45.32.107.195",
		"phone": p.GetString("phone.admin","081213631232"),
	}
}

type test_struct struct {
	BankCode string
	Name string
}

type VA struct{
	External_id string
	Bank_code string
	Name string
}

func main() {
	http.HandleFunc("/balance", balance)
	http.HandleFunc("/va/create", vaCreate)
	http.ListenAndServe(":7028", nil)
}

func initRedis() *redis.Client{
	p := properties.MustLoadFile("/etc/service.conf", properties.UTF8)
	address := p.GetString("redis.name","localhost")+":"+p.GetString("redis.port","6379")
	client := redis.NewClient(&redis.Options{
		Addr:     address,
		Password: p.GetString("redis.password",""),
		DB:       p.GetInt("redis.db",0),
	})
	return client
}

func balance(w http.ResponseWriter, r *http.Request) {
	param := initParam()
	client := &http.Client{}
	req, _ := http.NewRequest("GET", param["url"]+"balance", nil)
	req.SetBasicAuth(param["apiKey"], "")
	resp, err := client.Do(req)
	if err != nil{
		log.Fatal(err)
	}
	bodyText, err := ioutil.ReadAll(resp.Body)
	s := string(bodyText)
	bal, _ := jsonparser.GetInt(bodyText, "balance")
	var jsonprep string = `{"apikey":"`+param["apiKeySms"]+`", "callbackurl":"", "datapacket":[{"number":"`+param["phone"]+`","message":"Xendit Balance : `+strconv.FormatInt(bal,10)+`","sendingdatetime":"`+time.Now().Local().String()+`"}]}`
		var jsonStr = []byte(jsonprep)
		req, err = http.NewRequest("POST", param["urlSms"]+"/sms/api_sms_reguler_send_json.php", bytes.NewBuffer(jsonStr))
		req.Header.Set("Content-Type", "application/json")
		resp, err = client.Do(req)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("Unable to reach the server."))
		} else {
			_, _ = ioutil.ReadAll(resp.Body)
			w.WriteHeader(http.StatusOK)
			w.Write([]byte(s))
		}
}

func vaCreate(w http.ResponseWriter, r *http.Request) {
	body, _ := ioutil.ReadAll(r.Body)
	tmpPhone,_ := jsonparser.GetString(body, "phone")
	phone := validatePhoneNumber(tmpPhone)
	randVa := randInt(1038000,1038099)
	vaBNI,errBni := getVirtualAccount("BNI",phone,randVa)
	if errBni != nil{
		fmt.Print("BNI : ")
		fmt.Println(errBni)
		w.WriteHeader(401)
	}
	vaMandiri,errMandiri := getVirtualAccount("MANDIRI",phone,randVa)
	if errMandiri != nil{
		fmt.Print("Mandiri : ")
		fmt.Println(errMandiri)
		w.WriteHeader(401)
	}
	vaBca,errBca := getVirtualAccount("BCA",phone,randVa)
	if errBca != nil{
		fmt.Print("Mandiri : ")
		fmt.Println(errMandiri)
		w.WriteHeader(401)
	}

	va := `{"BNI":"`+vaBNI+`","BCA":"`+vaBca+`","Mandiri":"`+vaMandiri+`"}`
	w.WriteHeader(201)
	w.Write([]byte(va))

}

func validatePhoneNumber(number string) string{
	newNumber := ""
	if strings.HasPrefix(number, "+62"){
		newNumber = strings.Replace(number,"+62","0",1)
	}else if strings.HasPrefix(number, "62"){
		newNumber = strings.Replace(number,"62","0",-1)
	}else{
		newNumber = number
	}
	return newNumber
}

func getVirtualAccount(bankCode string,phone string,randVa int) (accountNumber string,err error){
	param := initParam()
	client := &http.Client{}

	ext_id := bankCode+strconv.Itoa(randVa)
	var jsonprep string = `{"external_id":"`+ext_id+`","bank_code":"`+ bankCode +`","name":"TOP UP DIGIROIN"}`
	var jsonStr = []byte(jsonprep)
	req, err := http.NewRequest("POST", param["url"]+"callback_virtual_accounts", bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")
	req.SetBasicAuth(param["apiKey"], "")
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Unable to reach the server.")
	} else {
		body, _ := ioutil.ReadAll(resp.Body)
		accountNumber, _ = jsonparser.GetString(body, "account_number")
		errorCode,_ := jsonparser.GetString(body, "error_code")
		if errorCode != ""{
			messages,_ := jsonparser.GetString(body, "message")
			err = errors.New(errorCode +" : "+ messages )
		}
	}
	return accountNumber,err
}

func randInt(min int, max int) int {
	rand.Seed(time.Now().UTC().UnixNano())
	return min + rand.Intn(max-min)
}